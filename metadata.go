package liber

import (
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/meskio/epubgo"
	"git.autistici.org/ale/liber/third_party/gobipocket"
)

// A metadata provider generates metadata from the local filesystem.
type MetadataProvider interface {
	Name() string
	Lookup(*FileStorage, string, string) (*Metadata, error)
	GetBookCover(*FileStorage, string) (string, error)
}

// A metadata refiner improves on existing metadata and may provide
// more than one result to choose from. It usually involves talking to
// a remote service.
type MetadataRefiner interface {
	Name() string
	Lookup(*Metadata) ([]*Metadata, error)
	GetBookCover(*Metadata) ([]byte, error)
}

type MetadataSource struct {
	Name string
	ID   string
}

type Metadata struct {
	Title       string
	Date        string
	Description string
	ISBN        []string
	Creator     []string
	Language    []string
	Publisher   []string
	Format      []string
	Keywords    []string
	Sources     []MetadataSource
}

// Sufficient returns true if the object contains enough information.
// If this check does not pass, the book won't be added to the database.
func (m *Metadata) Sufficient() bool {
	return m.Title != ""
}

// Complete returns true if we're satisfied with the quality of the
// information about this book. If this returns true, remote checks
// will be skipped.
func (m *Metadata) Complete() bool {
	return (m.Title != "" && len(m.Creator) > 0 && len(m.ISBN) > 0)
}

// Uniques returns the list of possible unique tokens for this book.
func (m *Metadata) Uniques() []string {
	var out []string
	for _, isbn := range m.ISBN {
		out = append(out, fmt.Sprintf("isbn:%s", isbn))
	}
	// Call it 'sig', but it's just title+author(s).
	sig := strings.ToLower(fmt.Sprintf("sig:%s|%s", m.Title, strings.Join(m.Creator, "|")))
	out = append(out, sig)
	return out
}

func listsOverlap(a, b []string) bool {
	for _, aa := range a {
		for _, bb := range b {
			if aa == bb {
				return true
			}
		}
	}
	return false
}

// Equals returns true if we think the two Metadata objects refer to
// the same book.
func (m *Metadata) Equals(other *Metadata) bool {
	// Compare ISBNs first, authors+title next.
	return (listsOverlap(m.ISBN, other.ISBN) ||
		(m.Title == other.Title && listsOverlap(m.Creator, other.Creator)))
}

// Merge with a more authoritative metadata source. Fields that are
// set in 'other' will take precedence over the ones in the receiver.
func (m *Metadata) Merge(other *Metadata) {
	if other.Title != "" {
		m.Title = other.Title
	}
	if other.Date != "" {
		m.Date = other.Date
	}
	if other.Description != "" {
		m.Description = other.Description
	}
	if other.Creator != nil {
		m.Creator = other.Creator
	}
	if other.ISBN != nil {
		m.ISBN = other.ISBN
	}
	if other.Publisher != nil {
		m.Publisher = other.Publisher
	}
	if other.Language != nil {
		m.Language = other.Language
	}
	if other.Format != nil {
		m.Format = other.Format
	}
	if other.Sources != nil {
		m.Sources = append(m.Sources, other.Sources...)
	}
}

var authorNameRx = regexp.MustCompile(`([\p{Lu}])[\p{Ll}]+ `)

func toYear(date string) string {
	if len(date) > 4 {
		return date[:4]
	}
	return date
}

func shortAuthorName(a string) string {
	return authorNameRx.ReplaceAllString(a, "$1. ")
}

func shortAuthorString(authors []string) string {
	var shortAuthors []string
	for _, a := range authors {
		shortAuthors = append(shortAuthors, shortAuthorName(a))
	}
	return strings.Join(shortAuthors, ", ")
}

func (m *Metadata) String() string {
	author := "unknown author"
	if len(m.Creator) > 0 {
		author = shortAuthorString(m.Creator)
	}
	if m.Date != "" {
		return fmt.Sprintf("%s, \"%s\", %s", author, m.Title, toYear(m.Date))
	}
	return fmt.Sprintf("%s, \"%s\"", author, m.Title)
}

func parseEpub(filename string) (*Metadata, error) {
	e, err := epubgo.Open(filename)
	if err != nil {
		return nil, err
	}
	defer e.Close()

	var m Metadata
	for _, field := range e.MetadataFields() {
		attr, err := e.MetadataAttr(field)
		if err != nil {
			continue
		}
		data, err := e.Metadata(field)
		if err != nil {
			continue
		}

		switch field {
		case "title":
			m.Title = data[0]
		case "date":
			// Just pick the first date available (year only).
			m.Date = toYear(data[0])
		case "creator":
			m.Creator = data
		case "language":
			m.Language = data
		case "publisher":
			m.Publisher = data
		case "identifier":
			for i, id := range data {
				if attr[i]["scheme"] == "ISBN" {
					m.ISBN = append(m.ISBN, id)
				}
			}
		}
	}

	// If we haven't found an ISBN, look for it in the book text.
	if len(m.ISBN) == 0 {
		if isbn := findISBNInEpub(e); len(isbn) > 0 {
			log.Printf("found ISBN in book text: %s", strings.Join(isbn, ", "))
			m.ISBN = isbn
		}
	}

	m.Sources = []MetadataSource{{
		Name: "epub",
		ID:   filename,
	}}

	return &m, nil
}

func parseMobi(filename string) (*Metadata, error) {
	e, err := mobipocket.Open(filename)
	if err != nil {
		return nil, err
	}

	var m Metadata
	for field, data := range e.Metadata {
		switch field {
		case "title":
			m.Title = data[0]
		case "pubdate":
			m.Date = toYear(data[0])
		case "description":
			m.Description = data[0]
		case "publisher":
			m.Publisher = data
		case "author":
			m.Creator = data
		case "isbn":
			m.ISBN = data
		}
	}

	m.Sources = []MetadataSource{{
		Name: "mobi",
		ID:   filename,
	}}

	return &m, nil
}

var alphanumRx = regexp.MustCompile(`[\pL\pN]+`)

func parseAnything(filename string) (*Metadata, error) {
	// Since there is no metadata available, try to match
	// something using just tokens from the file name.
	noext := strings.TrimSuffix(filepath.Base(filename), filepath.Ext(filename))
	return &Metadata{
		Keywords: alphanumRx.FindAllString(noext, -1),
	}, nil
}

type fileProvider struct{}

func (p *fileProvider) Lookup(storage *FileStorage, path, filetype string) (m *Metadata, err error) {
	// Catch panics from some of the underlying libraries (gmobi
	// in particular) and transform them into errors.
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic(): %v", r.(error))
		}
	}()

	path = storage.Abs(path)
	switch filetype {
	case ".epub":
		m, err = parseEpub(path)
	case ".mobi":
		m, err = parseMobi(path)
	case ".pdf":
		m, err = parseAnything(path)
	default:
		err = errors.New("unsupported file format")
	}

	return
}

func (p *fileProvider) GetBookCover(storage *FileStorage, path string) (string, error) {
	coverPath := path + ".cover.png"
	if storage.Exists(coverPath) {
		return coverPath, nil
	}
	return "", nil
}

func (p *fileProvider) Name() string {
	return "file"
}

func GetFileType(path string) (string, error) {
	filetype := strings.ToLower(filepath.Ext(path))
	if filetype != ".epub" && filetype != ".mobi" && filetype != ".pdf" {
		return "", errors.New("unsupported file format")
	}
	return filetype, nil
}
