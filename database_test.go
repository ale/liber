package liber

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"github.com/blevesearch/bleve/v2"
	blevequery "github.com/blevesearch/bleve/v2/search/query"
)

type testDatabase struct {
	db        *Database
	path      string
	refbookid BookId
}

func (td *testDatabase) Close() {
	td.db.Close()
	os.RemoveAll(td.path)
}

func newEmptyTestDatabase(t *testing.T) (*testDatabase, *Database) {
	path, _ := ioutil.TempDir("", "testdb-")
	db, err := NewDb(path)
	if err != nil {
		t.Fatalf("NewDb(): %v", err)
	}
	return &testDatabase{db: db, path: path}, db
}

func newTestDatabase(t *testing.T) (*testDatabase, *Database) {
	path, _ := ioutil.TempDir("", "testdb-")
	db, err := NewDb(path)
	if err != nil {
		t.Fatalf("NewDb(): %v", err)
	}

	book := testEbook()
	if err := db.PutBook(book); err != nil {
		t.Fatalf("PutBook(): %v", err)
	}
	if err := db.PutFile(testEpubFile(path, book.Id)); err != nil {
		t.Fatalf("PutFile(): %v", err)
	}

	return &testDatabase{db: db, path: path, refbookid: book.Id}, db
}

func newTestDatabase2(t *testing.T) (*testDatabase, *Database) {
	td, db := newTestDatabase(t)
	book := testEbook2()
	if err := db.PutBook(book); err != nil {
		t.Fatalf("PutBook(): %v", err)
	}
	return td, db
}

func testEpubFile(dir string, bookid BookId) *File {
	f, _ := ioutil.TempFile(dir, "ebook-")
	io.WriteString(f, "epub\n")
	f.Close()
	return &File{
		Id:       bookid,
		Path:     f.Name(),
		FileType: ".epub",
		Size:     4,
	}
}

func testEbook() *Book {
	return &Book{
		Id: NewID(),
		Metadata: &Metadata{
			Title:       "Twenty Thousand Leagues Under The Sea",
			Creator:     []string{"Jules Verne"},
			ISBN:        []string{"1234", "2345"},
			Description: "A pretty cool book.",
		},
	}
}

func testEbook2() *Book {
	return &Book{
		Id: NewID(),
		Metadata: &Metadata{
			Title:       "Around The World In Eighty Days",
			Creator:     []string{"Jules Verne"},
			ISBN:        []string{"5678"},
			Description: "It's about balloons.",
		},
	}
}

func TestDatabase_Get(t *testing.T) {
	td, db := newTestDatabase(t)
	defer td.Close()

	_, err := db.GetBook(td.refbookid)
	if err != nil {
		t.Fatalf("GetBook(%d): %v", td.refbookid, err)
	}
	files, err := db.GetBookFiles(td.refbookid)
	if err != nil {
		t.Fatalf("GetBookFiles(%d): %v", td.refbookid, err)
	}
	if len(files) != 1 {
		t.Fatalf("GetBookFiles(%d) bad result: %v", td.refbookid, files)
	}
}

func TestDatabase_BookFileRelation(t *testing.T) {
	td, db := newTestDatabase(t)
	defer td.Close()

	checkFiles := func(tag string, n int) []*File {
		files, err := db.GetBookFiles(td.refbookid)
		if err != nil {
			t.Fatalf("GetBookFiles@%s(%d): %v", tag, td.refbookid, err)
		}
		if len(files) != n {
			t.Fatalf("GetBookFiles@%s(%d) bad result (exp. len=%d): %v", tag, td.refbookid, n, files)
		}
		return files
	}

	files := checkFiles("init", 1)
	file0 := files[0]
	file1 := testEpubFile(td.path, td.refbookid)
	db.PutFile(file1)
	checkFiles("post_add", 2)

	db.DeleteFile(file1.Path)
	checkFiles("post_delete", 1)

	db.DeleteFile(file0.Path)
	checkFiles("post_delete_2", 0)

	if _, err := db.GetBook(td.refbookid); err == nil {
		t.Fatal("Book was not removed when n.files==0")
	}
}

func TestDatabase_BleveLowLevelSearch(t *testing.T) {
	td, db := newTestDatabase2(t)
	defer td.Close()

	doSearch := func(tag string, query blevequery.Query, numExpectedResults int) {
		req := bleve.NewSearchRequestOptions(query, 100, 0, false)
		result, err := db.index.Search(req)
		if err != nil {
			t.Fatalf("%s: %v", tag, err)
		}
		if int(result.Total) != numExpectedResults {
			t.Errorf("%s: got %d results, expected %d", tag, result.Total, numExpectedResults)
		}
	}

	matchQueryField := func(s, f string) blevequery.Query {
		q := bleve.NewMatchQuery(s)
		q.SetField(f)
		return q
	}
	termQueryField := func(t, f string) blevequery.Query {
		q := bleve.NewTermQuery(t)
		q.SetField(f)
		return q
	}

	doSearch("match_query",
		bleve.NewMatchQuery("Leagues Under The Sea"),
		1)
	doSearch("match_query_with_field",
		matchQueryField("Leagues Under The Sea", "title"),
		1)
	doSearch("match_query_with_field_2",
		matchQueryField("Jules Verne", "author"),
		2)
	//doSearch("match_query_with_field_2_AND",
	//	bleve.NewMatchQuery("Hugo Verne").SetField("author"),
	//	0)
	doSearch("match_query_with_wrong_field",
		matchQueryField("Leagues Under The Sea", "author"),
		0)
	doSearch("query_string_precise",
		bleve.NewQueryStringQuery("+title:Leagues +author:verne"),
		1)
	doSearch("isbn_term_query",
		termQueryField("1234", "isbn"),
		1)
	doSearch("isbn_term_query_2",
		termQueryField("2345", "isbn"),
		1)
}

func TestDatabase_Search(t *testing.T) {
	td, db := newTestDatabase2(t)
	defer td.Close()

	doSearch := func(query string, numExpectedResults int) {
		r, err := db.Search(query, 0, 100)
		if err != nil {
			t.Fatalf("Search(%s) failed: %v", query, err)
		}
		if r.NumResults != numExpectedResults {
			t.Errorf("Search(%s): got %d results, expecting %d\n%#v", query, r.NumResults, numExpectedResults, r.Results)
		}
	}

	doSearch("jules verne", 2)
	doSearch("italo calvino", 0)

	doSearch("Twenty Thousand Leagues", 1)
	doSearch("\"Twenty Thousand Leagues\"", 1)
	doSearch("title:\"Twenty Thousand Leagues\"", 1)
	doSearch("author:\"Jules Verne\"", 2)

	doSearch("author:verne", 2)
	doSearch("+title:Leagues +author:Verne", 1)
	doSearch("title:verne", 0)
	doSearch("author:vernes", 0)
}

func TestDatabase_Find(t *testing.T) {
	td, db := newTestDatabase2(t)
	defer td.Close()
	book, _ := db.GetBook(td.refbookid)
	m := book.Metadata

	// Find using ISBN.
	if result, err := db.Find(m.Uniques()); err != nil {
		t.Errorf("With ISBN: %v", err)
	} else if result.Id != book.Id {
		t.Errorf("Bad match with ISBN: got=%d, expected=%d", result.Id, book.Id)
	}

	// Find using title/author.
	m.ISBN = nil
	if result, err := db.Find(m.Uniques()); err != nil {
		t.Errorf("With title/author: %v", err)
	} else if result.Id != book.Id {
		t.Errorf("Bad match with title/author: got=%d, expected=%d", result.Id, book.Id)
	}

	// Find only using title (should fail).
	m.Creator = nil
	if result, err := db.Find(m.Uniques()); err == nil {
		t.Errorf("Title only: no error, result = %v", result)
	}
}

func TestDatabase_Reindex(t *testing.T) {
	td, db := newTestDatabase2(t)
	defer td.Close()
	book, _ := db.GetBook(td.refbookid)
	m := book.Metadata

	doTest := func() {
		// Find the first time.
		if result, err := db.Find(m.Uniques()); err != nil {
			t.Errorf("Not found: %v", err)
		} else if result.Id != book.Id {
			t.Errorf("Bad match with ISBN: got=%d, expected=%d", result.Id, book.Id)
		}
	}

	doTest()
	if err := db.Reindex(); err != nil {
		t.Fatalf("Reindex: %v", err)
	}
	doTest()
}

func TestDatabase_DumpAndRestore(t *testing.T) {
	td, db := newTestDatabase2(t)
	defer td.Close()
	book, _ := db.GetBook(td.refbookid)
	m := book.Metadata

	var buf bytes.Buffer
	if err := db.Dump(&buf); err != nil {
		t.Fatalf("Dump: %v", err)
	}

	td2, db2 := newEmptyTestDatabase(t)
	defer td2.Close()

	if err := db2.Restore(&buf); err != nil {
		t.Fatalf("Restore: %v", err)
	}

	// Find the sample book.
	if result, err := db2.Find(m.Uniques()); err != nil {
		t.Errorf("Not found: %v", err)
	} else if result.Id != book.Id {
		t.Errorf("Bad match with ISBN: got=%d, expected=%d", result.Id, book.Id)
	}
}

// func TestDatabase_Find2(t *testing.T) {
// 	td, db := newTestDatabase(t)
// 	defer td.Close()

// 	testdata := []*Metadata{
// 		&Metadata{Title: "PHILIP K", Creator: []string{"Antonio Marigonda"}, Language: []string{"it"}, Sources: []MetadataSource{MetadataSource{Name: "opf", ID: "6809f8b6-3151-41ef-b05a-0eb113746674"}}},
// 	}

// 	for _, m := range testdata {
// 		t.Logf("uniques = %v", m.Uniques())
// 		id := NewID()
// 		db.PutBook(&Book{Id: id, Metadata: m})
// 		if _, err := db.Find(m.Uniques()); err != nil {
// 			t.Errorf("Could not find book: %#v", m)
// 		}
// 	}
// }

func TestDatabase_Suggest(t *testing.T) {
	td, db := newTestDatabase(t)
	defer td.Close()

	for _, s := range []string{"jul", "jule", "jules", "ver", "vern", "verne", "twent", "thous"} {
		r, err := db.Suggest(s)
		if err != nil {
			t.Fatal(err)
		}
		if r.NumResults == 0 {
			t.Errorf("No results for '%s'", s)
		}
	}

	r, err := db.Suggest("foo")
	if err != nil {
		t.Fatal(err)
	}
	if r.NumResults != 0 {
		t.Errorf("Unexpected results: %v", r)
	}
}
