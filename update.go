package liber

import (
	"errors"
	"log"
	"os"
	"sync"

	"git.autistici.org/ale/liber/util"
)

const (
	SourceDB = 1 << iota
	SourceFS
)

type MetadataChooserFunc func(string, []*Metadata) *Metadata

type fileData struct {
	source   int
	path     string
	filetype string
	id       BookId
	info     os.FileInfo
}

func (f fileData) toLiberFile(storage *FileStorage, haserr bool) (*File, error) {
	return &File{
		Path:     f.path,
		FileType: f.filetype,
		Mtime:    f.info.ModTime(),
		Size:     f.info.Size(),
		Id:       f.id,
		Error:    haserr,
	}, nil
}

type fileAndBook struct {
	f fileData
	b *Book
}

type updateContext struct {
	db        *Database
	storage   *FileStorage
	chooser   MetadataChooserFunc
	providers []MetadataProvider
	refiners  []MetadataRefiner
}

func (uc *updateContext) dbFileScanner(fileCh chan fileData) {
	iter := uc.db.Scan(FileBucket)
	defer iter.Close()
	for iter.Next() {
		var f File
		if err := iter.Value(&f); err != nil {
			continue
		}
		fileCh <- fileData{
			source: SourceDB,
			path:   f.Path,
			id:     f.Id,
		}
	}
}

func (uc *updateContext) localFileScanner(basedir string, fileCh chan fileData) {
	uc.storage.Walk(util.NewDefaultWalker(), func(path string, info os.FileInfo, err error) error {
		fileCh <- fileData{
			source: SourceFS,
			path:   path,
			info:   info,
		}
		return nil
	})
}

func (uc *updateContext) differ(basedir string) chan fileData {
	fileCh := make(chan fileData, 100)
	outCh := make(chan fileData, 100)
	var wg sync.WaitGroup
	wg.Add(2)

	// Start two sources in parallel and send their output to fileCh.
	go func() {
		uc.localFileScanner(basedir, fileCh)
		wg.Done()
	}()
	go func() {
		uc.dbFileScanner(fileCh)
		wg.Done()
	}()
	// Once they are done, close the channel.
	go func() {
		wg.Wait()
		close(fileCh)
	}()
	go func() {
		// Merge the two sources and keep track of files that
		// only appear in the database but not on the
		// filesystem, so we can remove them at the end.
		// All entries with source == SourceFS will be sent to
		// the output channel in any case.
		allSources := SourceDB | SourceFS
		tmp := make(map[string]int)
		for f := range fileCh {
			tmp[f.path] |= f.source
			// Delete entries as soon as we've seen them
			// originate from both sources.
			if tmp[f.path] == allSources {
				delete(tmp, f.path)
			}
			if f.source == SourceFS {
				outCh <- f
			}
		}
		for path, value := range tmp {
			if value == SourceDB {
				log.Printf("file %s has been removed", path)
				uc.db.DeleteFile(path)
			}
		}
		close(outCh)
	}()
	return outCh
}

func (uc *updateContext) extractor(fileCh chan fileData, outCh chan fileAndBook) {
	for f := range fileCh {
		if oldfile, err := uc.db.GetFile(f.path); err == nil {
			if !oldfile.HasChanged(f.info) {
				continue
			}
			f.id = oldfile.Id
		}
		book, filetype, err := uc.parseMeta(f)
		if err == nil {
			f.filetype = filetype
			outCh <- fileAndBook{f: f, b: book}
			continue
		}

		// Parse errors are permanent.
		log.Printf("Could not parse %s: %v", f.path, err)
		file, err := f.toLiberFile(uc.storage, true)
		if err != nil {
			log.Printf("Error saving file %s: %v", file.Path, err)
			continue
		}
		if err := uc.db.PutFile(file); err != nil {
			log.Printf("Error saving file %s to db: %v", file.Path, err)
		}
	}
}

func (uc *updateContext) parseMeta(f fileData) (*Book, string, error) {
	filetype, err := GetFileType(f.path)
	if err != nil {
		return nil, "", err
	}

	// Attempt metadata extraction from the providers. The first
	// match returned stops the iteration. At the same time, look
	// for a cover image until one is found.
	var meta *Metadata
	var coverPath string

	for _, provider := range uc.providers {
		if meta == nil {
			meta, err = provider.Lookup(uc.storage, f.path, filetype)
			if err != nil {
				log.Printf("%s: %s: could not parse: %v", f.path, provider.Name(), err)
			} else if meta != nil {
				log.Printf("%s: identified by: %s", f.path, provider.Name())
			}
		}
		if coverPath == "" {
			coverPath, err = provider.GetBookCover(uc.storage, f.path)
			if err != nil {
				log.Printf("%s: %s: could not fetch cover image at %s", f.path, provider.Name(), err)
			} else if coverPath != "" {
				log.Printf("%s: cover image found by: %s", f.path, provider.Name())
			}
		}
	}
	if meta == nil {
		return nil, "", errors.New("no metadata could be identified")
	}

	// Create a Book with no ID (yet).
	book := &Book{
		Metadata:  meta,
		CoverPath: coverPath,
	}

	// Only run remote checks if the metadata isn't complete.
	if !meta.Complete() {

		// Integrate metadata using the refiners. We check them all,
		// and merge their results into the metadata object. The user
		// is prompted if a choice is necessary. Search for a book
		// cover only until one is found.
		for _, refiner := range uc.refiners {
			candidates, err := refiner.Lookup(meta)
			if err == nil && len(candidates) > 0 {
				if len(candidates) == 1 {
					log.Printf("found match from %s: %s", refiner.Name(), candidates[0].String())
					meta.Merge(candidates[0])
				} else if uc.chooser != nil {
					if userchoice := uc.chooser(f.path, candidates); userchoice != nil {
						meta.Merge(userchoice)
					}
				}
			}
		}
	}

	// Check if the book metadata looks ok. If not, don't even
	// bother looking for a cover image.
	if !meta.Sufficient() {
		return nil, "", errors.New("insufficient metadata")
	}

	// Errors finding/saving cover images are not fatal.
	if err := findCoverImage(book, f.path, uc.refiners, uc.storage); err != nil {
		log.Printf("Error saving cover image: %v", err)
	}

	return book, filetype, nil
}

func (uc *updateContext) dbwriter(ch chan fileAndBook) {
	for pair := range ch {
		saveBook := true

		// If this is a new file, see if it matches an already
		// existing book.
		if pair.f.id == 0 {
			log.Printf("potential new book: %#v", pair.b.Metadata)
			if match, err := uc.db.Find(pair.b.Metadata.Uniques()); err == nil {
				log.Printf("%s matches existing book %d", pair.f.path, match.Id)
				// Ignore new metadata.
				pair.b = match
				saveBook = false
			} else {
				// Assign a new ID to the book.
				pair.b.Id = NewID()
			}
			pair.f.id = pair.b.Id
		} else {
			// Overwrite the old book metadata.
			pair.b.Id = pair.f.id
		}

		if saveBook {
			if err := uc.db.PutBook(pair.b); err != nil {
				log.Printf("Error saving book %d to db: %v", pair.b.Id, err)
				continue
			}
			log.Printf("%s -> %d", pair.f.path, pair.b.Id)
		}

		file, err := pair.f.toLiberFile(uc.storage, false)
		if err != nil {
			log.Printf("Error saving file %s: %v", pair.f.path, err)
			continue
		}
		if err := uc.db.PutFile(file); err != nil {
			log.Printf("Error saving file %s to db: %v", file.Path, err)
		}
	}
}

var (
	defaultMetadataProviders = []MetadataProvider{
		// Calibre/OPF must be first, so we don't attempt to
		// parse the file itself if we have external metadata.
		&opfProvider{},
		&fileProvider{},
	}

	defaultMetadataRefiners = []MetadataRefiner{
		// Check Google Books when the metadata is not
		// sufficient to fully describe the book.
		&googleBooksRefiner{},
		//&openLibraryRefiner{},
	}
)

const numUpdateMetadataWorkers = 10

func (db *Database) Update(dir string, chooser MetadataChooserFunc) {
	// Parallelize metadata extraction, serialize database updates
	// (so that index-based de-duplication works).
	uc := &updateContext{
		db:        db,
		chooser:   chooser,
		storage:   NewFileStorage(dir),
		providers: defaultMetadataProviders,
		refiners:  defaultMetadataRefiners,
	}

	var wg sync.WaitGroup
	ch := uc.differ(dir)
	pch := make(chan fileAndBook)
	for i := 0; i < numUpdateMetadataWorkers; i++ {
		wg.Add(1)
		go func() {
			uc.extractor(ch, pch)
			wg.Done()
		}()
	}
	go func() {
		wg.Wait()
		close(pch)
	}()
	uc.dbwriter(pch)
}
