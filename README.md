liber
=====

Software to manage an ebook collection. It supports EPUB, MOBI and PDF
formats, and it will fetch metadata and cover images on external
sources (currently Google Books). It offers powerful search
functionality over a HTTP interface.

Another useful feature is the ability to synchronize remote
collections (currently in one direction only), making it possible for
a group of people to manage a centralized ebook repository.


## Installation

Binaries are available in the form of Debian packages. They will take
care of installing all the necessary dependencies. If this isn't an
option, you can build the software from source.

### Debian packages

Add this line to your `sources.list`:

    deb [signed-by=/usr/share/keyrings/incal-archive-keyring.gpg] http://debian.incal.net/debian liber/

Then make sure the GPG key used to sign the repository is installed,
update the package sources, and install the `liber` package:

    $ gpg --keyserver hkp://keys.openpgp.org/ --recv 06F524BFE221CD1F65DF57F5C0EAC2F9CE9ED9B0
    $ gpg --export --export-options export-minimal 06F524BFE221CD1F65DF57F5C0EAC2F9CE9ED9B0 | sudo tee /usr/share/keyrings/incal-archive-keyring.gpg > /dev/null
    $ sudo apt-get update
    $ sudo apt-get install liber

### Build from source

To compile liber from source, you will need a working [Go](http://golang.org/)
environment (note that the Go version in Debian wheezy is too old, you should
install a more recent version from the Go website).

Building and installing the code is the simply a matter of running

    $ go get -d git.autistici.org/ale/liber
    $ go install git.autistici.org/ale/liber/...

This will place the resulting `liber` executable in `$GOPATH/bin`.


## Usage

`liber` will store its database in a local directory, `~/.liber` by
default. Use the `--db-dir` option if you'd like to change this.

If you want to manage a local collection, the assumption is that you
are storing all your ebooks in a single place (below a single
directory, possibly organized into further subdirectories). You can
control this with the (mandatory) option `--book-dir`.

### Indexing a local ebook collection

To index a local ebook collection, run the following command:

    $ liber --book-dir=/path/to/ebooks update

The tool will attempt to identify books on Google Books. It is
possible that more than one match is found, in which case `liber` will
open a dialog box to ask you interactively to pick the right match.

You can run `liber update` as many times as you like (for example
whenever you add an ebook to your collection), it will automatically
detect new files and files that have been removed.

#### Integration with Calibre

If you use Calibre to mantain your ebook collection, you can index it
with `liber` by simply pointing its `--book-dir` option at the Calibre
library directory. `liber` will read Calibre metadata files and cover
images, and it will not perform remote searches for book metadata.

### Searching

You can search the index from the command line, for example:

    $ liber search "Das Kapital"

This will print a list of documents that match the query. For the full
query syntax, see
[the Bleve documentation](https://github.com/blevesearch/bleve/wiki/Query-String-Query).

### Synchronizing with a remote server

To upload the contents of the local database (including the file
contents themselves) to a remote `liber` server, run the following
command:

    $ liber sync http://remote.server.address/


### Running the HTTP interface

The HTTP interface can be started with:

    $ liber --book-dir=/path/to/ebooks server

This will start an HTTP server on port 3000, listening on all
interfaces. The HTTP server needs some templates and static content
which the Debian package installs in `/usr/share/liber/htdocs`.

The HTTP server will store uploaded files into the directory specified
by `--book-dir`. You should use the same value that you passed to
`liber update`.
