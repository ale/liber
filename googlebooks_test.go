package liber

import (
	"reflect"
	"strings"
	"testing"
)

var testGbXml = `<?xml version='1.0' encoding='utf-8'?>
<feed xmlns='http://www.w3.org/2005/Atom'
xmlns:openSearch='http://a9.com/-/spec/opensearchrss/1.0/'
xmlns:gbs='http://schemas.google.com/books/2008'
xmlns:dc='http://purl.org/dc/terms'
xmlns:batch='http://schemas.google.com/gdata/batch'
xmlns:gd='http://schemas.google.com/g/2005'>
  <id>http://www.google.com/books/feeds/volumes</id>
  <updated>2014-11-07T09:50:21.000Z</updated>
  <category scheme='http://schemas.google.com/g/2005#kind'
  term='http://schemas.google.com/books/2008#volume' />
  <title type='text'>Search results for intitle:cosmicomiche inauthor:calvino</title>
  <link rel='alternate' type='text/html' href='http://www.google.com' />
  <link rel='http://schemas.google.com/g/2005#feed' type='application/atom+xml'
  href='http://www.google.com/books/feeds/volumes' />
  <link rel='self' type='application/atom+xml' href='http://www.google.com/books/feeds/volumes?q=intitle%3Acosmicomiche+inauthor%3Acalvino' />
  <author>
    <name>Google Books Search</name>
    <uri>http://www.google.com</uri>
  </author>
  <generator version='beta'>Google Book Search data API</generator>
  <openSearch:totalResults>1</openSearch:totalResults>
  <openSearch:startIndex>1</openSearch:startIndex>
  <openSearch:itemsPerPage>1</openSearch:itemsPerPage>
  <entry>
    <id>http://www.google.com/books/feeds/volumes/Q3m_MA55v5QC</id>
    <updated>2014-11-07T09:50:21.000Z</updated>
    <category scheme='http://schemas.google.com/g/2005#kind'
    term='http://schemas.google.com/books/2008#volume' />
    <title type='text'>Le cosmicomiche</title>
    <link rel='http://schemas.google.com/books/2008/thumbnail' type='image/x-unknown' href='http://bks7.books.google.com/books?id=Q3m_MA55v5QC&amp;printsec=frontcover&amp;img=1&amp;zoom=5&amp;edge=curl&amp;source=gbs_gdata' />
    <link rel='http://schemas.google.com/books/2008/info' type='text/html' href='http://books.google.com/books?id=Q3m_MA55v5QC&amp;dq=intitle:cosmicomiche+inauthor:calvino&amp;ie=ISO-8859-1&amp;source=gbs_gdata' />
    <link rel='http://schemas.google.com/books/2008/preview' type='text/html' href='http://books.google.com/books?id=Q3m_MA55v5QC&amp;printsec=frontcover&amp;dq=intitle:cosmicomiche+inauthor:calvino&amp;ie=ISO-8859-1&amp;cd=2&amp;source=gbs_gdata' />
    <link rel='http://schemas.google.com/books/2008/annotation' type='application/atom+xml' href='http://www.google.com/books/feeds/users/me/volumes' />
    <link rel='alternate' type='text/html' href='http://books.google.com/books?id=Q3m_MA55v5QC&amp;dq=intitle:cosmicomiche+inauthor:calvino&amp;ie=ISO-8859-1' />
    <link rel='self' type='application/atom+xml' href='http://www.google.com/books/feeds/volumes/Q3m_MA55v5QC' />
    <gbs:contentVersion>1.6.5.0.preview.3</gbs:contentVersion>
    <gbs:embeddability value='http://schemas.google.com/books/2008#embeddable' />
    <gbs:openAccess value='http://schemas.google.com/books/2008#disabled' />
    <gbs:viewability value='http://schemas.google.com/books/2008#view_partial' />
    <dc:creator>Italo Calvino</dc:creator>
    <dc:date>2013-04-16</dc:date>
    <dc:format>196 pages</dc:format>
    <dc:format>book</dc:format>
    <dc:identifier>Q3m_MA55v5QC</dc:identifier>
    <dc:identifier>ISBN:9788852036361</dc:identifier>
    <dc:identifier>ISBN:8852036369</dc:identifier>
    <dc:language>it</dc:language>
    <dc:publisher>Edizioni Mondadori</dc:publisher>
    <dc:subject>Fiction</dc:subject>
    <dc:title>Le cosmicomiche</dc:title>
  </entry>
</feed>
`

func TestGoogleBooks_Parse(t *testing.T) {
	results, err := googleBooksParse(strings.NewReader(testGbXml))
	if err != nil {
		t.Fatal(err)
	}
	if len(results) != 1 {
		t.Fatalf("Unexpected results: %#v", results)
	}

	expected := &Metadata{
		Title:     "Le cosmicomiche",
		Date:      "2013",
		ISBN:      []string{"9788852036361", "8852036369"},
		Creator:   []string{"Italo Calvino"},
		Language:  []string{"it"},
		Publisher: []string{"Edizioni Mondadori"},
		Format:    []string{"196 pages", "book"},
		Sources: []MetadataSource{{
			Name: "gbooks",
			ID:   "http://www.google.com/books/feeds/volumes/Q3m_MA55v5QC",
		}},
	}

	if !reflect.DeepEqual(results[0], expected) {
		t.Fatalf("Result does not match: expected=%#v, got=%#v", expected, results[0])
	}
}
