package util

import (
	"os/user"
	"path/filepath"
	"strings"
)

// ExpandTilde replaces an initial "~/" in the given path with the
// current user's home directory. It is intended as a convenience for
// those cases where the provided path does not come from a shell,
// which would otherwise handle the expansion.
func ExpandTilde(path string) string {
	if strings.HasPrefix(path, "~/") {
		if u, err := user.Current(); err == nil {
			return filepath.Join(u.HomeDir, path[2:])
		}
	}
	return path
}
