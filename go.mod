module git.autistici.org/ale/liber

go 1.14

require (
	github.com/blevesearch/bleve/v2 v2.3.6
	github.com/glycerine/go-unsnap-stream v0.0.0-20190901134440-81cf024a9e0a // indirect
	github.com/glycerine/goconvey v0.0.0-20190410193231-58a59202ab31 // indirect
	github.com/google/subcommands v1.2.0
	github.com/gopherjs/gopherjs v0.0.0-20190910122728-9d188e94fb99 // indirect
	github.com/gorilla/mux v1.8.1
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/meskio/epubgo v0.0.0-20160213181628-90dd5d78197f
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/syndtr/goleveldb v1.0.1-0.20220721030215-126854af5e6d
	github.com/tinylib/msgp v1.1.5 // indirect
	github.com/willf/bitset v1.1.11 // indirect
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
)
