package liber

import (
	"reflect"
	"strings"
	"testing"
)

var testOpf = `<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">3</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">96e4a1ff-dd24-4966-8fe9-2d16a14b9fb0</dc:identifier>
        <dc:title>Cypherpunks: Freedom and the Future of the Internet</dc:title>
        <dc:creator opf:file-as="Julian Assange, Jacob Appelbaum, Andy Muller-Maguhn, Jeremie Zimmermann" opf:role="aut">Julian Assange, Jacob Appelbaum, Andy Muller-Maguhn, Jeremie Zimmermann</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (0.9.18) [http://calibre-ebook.com]</dc:contributor>
        <dc:date>2012-11-25T23:00:00+00:00</dc:date>
        <dc:description>&lt;div&gt;&lt;p class="description"&gt;The harassment of WikiLeaks and other Internet activists, together with attempts to introduce anti-file sharing legislation such as SOPA and ACTA, indicate that the politics of the Internet have reached a crossroads. In one direction lies a future that guarantees, in the watchwords of the cypherpunks, “privacy for the weak and transparency for the powerful”; in the other lies an Internet that allows government and large corporations to discover ever more about internet users while hiding their own activities. Assange and his co-discussants unpick the complex issues surrounding this crucial choice with clarity and engaging enthusiasm.&lt;/p&gt;&lt;p class="description"&gt;released by the CypherTeam&lt;/p&gt;&lt;/div&gt;</dc:description>
        <dc:publisher>OR Books</dc:publisher>
        <dc:identifier opf:scheme="MOBI-ASIN">6de78a57-3b52-45fe-9670-5621d44582d7</dc:identifier>
        <dc:identifier opf:scheme="ISBN">9781939293015</dc:identifier>
        <dc:language>fra</dc:language>
        <dc:subject>Bisac Code 1: POL039000</dc:subject>
        <meta content="{&quot;Julian Assange, Jacob Appelbaum, Andy Muller-Maguhn, Jeremie Zimmermann&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2013-08-24T12:13:18+00:00" name="calibre:timestamp"/>
        <meta content="Cypherpunks: Freedom and the Future of the Internet" name="calibre:title_sort"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Cover" type="cover"/>
    </guide>
</package>
`

func TestOpf_Parse(t *testing.T) {
	result, err := opfParse(strings.NewReader(testOpf))
	if err != nil {
		t.Fatal(err)
	}

	expected := &Metadata{
		Title:       "Cypherpunks: Freedom and the Future of the Internet",
		Date:        "2012",
		Description: "<div><p class=\"description\">The harassment of WikiLeaks and other Internet activists, together with attempts to introduce anti-file sharing legislation such as SOPA and ACTA, indicate that the politics of the Internet have reached a crossroads. In one direction lies a future that guarantees, in the watchwords of the cypherpunks, “privacy for the weak and transparency for the powerful”; in the other lies an Internet that allows government and large corporations to discover ever more about internet users while hiding their own activities. Assange and his co-discussants unpick the complex issues surrounding this crucial choice with clarity and engaging enthusiasm.</p><p class=\"description\">released by the CypherTeam</p></div>",
		ISBN:        []string{"9781939293015"},
		Creator:     []string{"Julian Assange", "Jacob Appelbaum", "Andy Muller-Maguhn", "Jeremie Zimmermann"},
		Publisher:   []string{"OR Books"},
		Language:    []string{"fra"},
		Sources: []MetadataSource{{
			Name: "opf",
			ID:   "96e4a1ff-dd24-4966-8fe9-2d16a14b9fb0",
		}},
	}
	if !reflect.DeepEqual(result, expected) {
		t.Fatalf("Result does not match: expected=%#v, got=%#v", expected, result)
	}
}
