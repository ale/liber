package main

import (
	"context"
	"flag"
	"log"
	"os"

	"github.com/google/subcommands"

	"git.autistici.org/ale/liber/util"
)

type refineCommand struct {
	noninteractive bool
}

func (c *refineCommand) SetFlags(f *flag.FlagSet) {
	f.BoolVar(&c.noninteractive, "noninteractive", false, "disable user prompts")
}

func (c *refineCommand) Name() string     { return "refine" }
func (c *refineCommand) Synopsis() string { return "Improve metadata" }
func (c *refineCommand) Usage() string {
	return `refine [<OPTIONS>]
  Improve metadata of books already in the database.

  Expects a list of book IDs on standard input.

`
}

func (c *refineCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	db := openDB()
	defer db.Close()

	if err := db.WithBookIDs(os.Stdin, db.RefineFunc(util.ExpandTilde(*bookDir), promptUser(c.noninteractive))); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}
