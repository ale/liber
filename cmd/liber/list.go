package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"git.autistici.org/ale/liber"

	"github.com/google/subcommands"
)

type listCommand struct{}

func (c *listCommand) Name() string             { return "list-books" }
func (c *listCommand) Synopsis() string         { return "List books that match certain criteria" }
func (c *listCommand) SetFlags(f *flag.FlagSet) {}
func (c *listCommand) Usage() string {
	return `list-books <CRITERIA>
  List books that match certain criteria. Known tags you can use:

    +all
    +incomplete
    +invalid
    +source:<SOURCE>
    -source:<SOURCE>
    +nofiles
    +noisbn
    +missingcover

`
}

func matchSource(source string) func(*liber.Book) bool {
	return func(book *liber.Book) bool {
		if book.Metadata == nil {
			return false
		}
		for _, msrc := range book.Metadata.Sources {
			if msrc.Name == source {
				return true
			}
		}
		return false
	}
}

func matchMissingSource(source string) func(*liber.Book) bool {
	return func(book *liber.Book) bool {
		if book.Metadata == nil {
			return true
		}
		for _, msrc := range book.Metadata.Sources {
			if msrc.Name == source {
				return false
			}
		}
		return true
	}
}

func matchNoFiles(db *liber.Database) func(*liber.Book) bool {
	return func(book *liber.Book) bool {
		f, _ := db.GetBookFiles(book.Id)
		return len(f) == 0
	}
}

func matchMissingCover(book *liber.Book) bool {
	if book.CoverPath == "" {
		return true
	}
	_, err := os.Stat(book.CoverPath)
	return err != nil && os.IsNotExist(err)
}

func matchIncomplete(book *liber.Book) bool {
	return (book.Metadata != nil && book.Metadata.Complete())
}

func matchNoISBN(book *liber.Book) bool {
	return book.Metadata == nil || len(book.Metadata.ISBN) == 0
}

func matchInvalid(_ *liber.Book) bool {
	return false
}

func matchAll(_ *liber.Book) bool {
	return true
}

func (c *listCommand) parseTag(db *liber.Database, tag string) (func(*liber.Book) bool, error) {
	if strings.HasPrefix(tag, "+source:") {
		return matchSource(tag[8:]), nil
	}
	if strings.HasPrefix(tag, "-source:") {
		return matchMissingSource(tag[8:]), nil
	}

	switch tag {
	case "+all":
		return matchAll, nil
	case "+incomplete":
		return matchIncomplete, nil
	case "+invalid":
		return matchInvalid, nil
	case "+nofiles":
		return matchNoFiles(db), nil
	case "+missingcover":
		return matchMissingCover, nil
	case "+noisbn":
		return matchNoISBN, nil
	default:
		return nil, fmt.Errorf("unknown tag '%s'", tag)
	}
}

func (c *listCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() == 0 {
		log.Printf("Not enough arguments")
		return subcommands.ExitUsageError
	}

	db := openDB()
	defer db.Close()

	var matchFuncs []func(*liber.Book) bool
	for _, arg := range f.Args() {
		f, err := c.parseTag(db, arg)
		if err != nil {
			log.Printf("error: %v", err)
			return subcommands.ExitUsageError
		}
		matchFuncs = append(matchFuncs, f)
	}

	db.ListBooks(os.Stdout, matchFuncs...)

	return subcommands.ExitSuccess
}
