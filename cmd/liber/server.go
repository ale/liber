package main

import (
	"context"
	"flag"
	"log"
	"path/filepath"

	"github.com/google/subcommands"

	"git.autistici.org/ale/liber"
	"git.autistici.org/ale/liber/util"
)

type serverCommand struct {
	addr string
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "Run the HTTP server" }
func (c *serverCommand) Usage() string {
	return `server [<OPTIONS>]
  Run the HTTP server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.addr, "addr", ":3001", "address to listen on")
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	db := openDB()
	defer db.Close()

	storage := liber.NewRWFileStorage(util.ExpandTilde(*bookDir), 2)
	cache := liber.NewRWFileStorage(filepath.Join(util.ExpandTilde(*databaseDir), "cache"), 2)
	server := liber.NewHttpServer(db, storage, cache, c.addr)
	if err := server.ListenAndServe(); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}
