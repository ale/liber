package main

import (
	"context"
	"flag"
	"log"
	"os"

	"github.com/google/subcommands"

	"git.autistici.org/ale/liber"
	"git.autistici.org/ale/liber/util"
)

var (
	databaseDir = flag.String("db-dir", "~/.liber", "database directory")
	bookDir     = flag.String("book-dir", "", "books directory")
)

func openDB() *liber.Database {
	dbdir := util.ExpandTilde(*databaseDir)
	db, err := liber.NewDb(dbdir)
	if err != nil {
		log.Fatal("error opening database: ", err)
	}
	return db
}

func main() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")
	subcommands.Register(&updateCommand{}, "")
	subcommands.Register(&searchCommand{}, "")
	subcommands.Register(&syncCommand{}, "")
	subcommands.Register(&serverCommand{}, "")
	subcommands.Register(&dumpCommand{}, "Maintenance")
	subcommands.Register(&restoreCommand{}, "Maintenance")
	subcommands.Register(&reindexCommand{}, "Maintenance")
	subcommands.Register(&refineCommand{}, "Maintenance")
	subcommands.Register(&listCommand{}, "Maintenance")

	log.SetFlags(0)
	flag.Parse()

	os.Exit(int(subcommands.Execute(context.Background())))
}
