package main

import (
	"context"
	"flag"
	"log"

	"github.com/google/subcommands"

	"git.autistici.org/ale/liber"
	"git.autistici.org/ale/liber/util"
)

type syncCommand struct{}

func (c *syncCommand) SetFlags(f *flag.FlagSet) {}
func (c *syncCommand) Name() string             { return "sync" }
func (c *syncCommand) Synopsis() string         { return "Synchronize with remote database" }
func (c *syncCommand) Usage() string {
	return `sync <URL>
  Push local content to a remote database.

`
}

func (c *syncCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() == 0 {
		log.Printf("Must specify the remote URL")
		return subcommands.ExitUsageError
	} else if f.NArg() > 1 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	db := openDB()
	defer db.Close()

	storage := liber.NewFileStorage(util.ExpandTilde(*bookDir))
	sc := liber.NewRemoteServer(f.Arg(0))
	if err := db.Sync(storage, sc); err != nil {
		log.Printf("sync failed: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}
