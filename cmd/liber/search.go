package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"strings"

	"github.com/google/subcommands"
)

type searchCommand struct{}

func (c *searchCommand) SetFlags(f *flag.FlagSet) {}
func (c *searchCommand) Name() string             { return "search" }
func (c *searchCommand) Synopsis() string         { return "Search the database" }
func (c *searchCommand) Usage() string {
	return `search <QUERY>
  Search the local database.

`
}

func (c *searchCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	query := strings.Join(f.Args(), " ")
	if query == "" {
		log.Printf("Must specify a query")
		return subcommands.ExitUsageError
	}

	db := openDB()
	defer db.Close()

	results, err := db.Search(query, 0, 100)
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	if results.NumResults == 0 {
		fmt.Printf("No results.\n")
	} else {
		fmt.Printf("%d results found:\n\n", results.NumResults)
		for i, book := range results.Results {
			fmt.Printf("%d) %s\n", i+1, book.Metadata.String())
			if files, err := db.GetBookFiles(book.Id); err == nil {
				for _, f := range files {
					fmt.Printf("      %s: %s\n", strings.TrimPrefix(f.FileType, "."), f.Path)
				}
			}
			fmt.Printf("\n")
		}
	}

	return subcommands.ExitSuccess
}
