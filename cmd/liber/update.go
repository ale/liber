package main

import (
	"context"
	"flag"
	"log"
	"os"
	"path/filepath"

	"github.com/google/subcommands"

	"git.autistici.org/ale/liber/util"
)

type updateCommand struct {
	noninteractive bool
}

func (c *updateCommand) SetFlags(f *flag.FlagSet) {
	f.BoolVar(&c.noninteractive, "noninteractive", false, "disable user prompts")
}

func (c *updateCommand) Name() string     { return "update" }
func (c *updateCommand) Synopsis() string { return "Add books to the local db" }
func (c *updateCommand) Usage() string {
	return `update [<OPTIONS>]
  Add books to the local database.

`
}

func (c *updateCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("Too many arguments")
		return subcommands.ExitUsageError
	}

	db := openDB()
	defer db.Close()

	// Redirect logging to dbdir/update.log.
	logf, err := os.OpenFile(filepath.Join(*databaseDir, "update.log"), os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err == nil {
		defer logf.Close()
		log.SetOutput(logf)
		log.SetFlags(log.Ldate | log.Ltime)
	}

	db.Update(util.ExpandTilde(*bookDir), promptUser(c.noninteractive))

	return subcommands.ExitSuccess
}
