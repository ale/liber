package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"

	"git.autistici.org/ale/liber"
)

// Various ways to ask a user to choose something.

// Only make one user prompt at a time.
var promptMutex sync.Mutex

// Prompt user using stdin. Kind of annoying because it interferes
// with logging on stderr. It is used as a fallback.
func promptUserStdin(path string, choices []*liber.Metadata) *liber.Metadata {
	promptMutex.Lock()
	defer promptMutex.Unlock()

	fmt.Printf("\n[*] Possible matches for %s:\n\n", path)
	for idx, md := range choices {
		fmt.Printf("    %d) %s\n", idx+1, md.String())
	}
	prompt := "Pick one, or Enter to skip: "

	rdr := bufio.NewReader(os.Stdin)
	for {
		fmt.Printf(prompt)
		os.Stdout.Sync()
		result, err := rdr.ReadString('\n')
		if err != nil || result == "\n" {
			break
		}
		idx, err := strconv.Atoi(strings.TrimSpace(result))
		if err != nil {
			fmt.Printf("%v\n", err)
			continue
		}
		if idx < 1 || idx > len(choices) {
			fmt.Printf("Insert a number between 1 and %d.\n", len(choices))
			continue
		}
		return choices[idx-1]
	}
	return nil
}

func findProgram(progs []string) (string, error) {
	for _, p := range progs {
		if path, err := exec.LookPath(p); err == nil {
			return path, nil
		}
	}
	return "", errors.New("not found")
}

var dialogProg string

func findDialogProg() bool {
	dialogProgs := []string{"whiptail", "dialog"}
	if os.Getenv("DISPLAY") != "" {
		dialogProgs = append([]string{"gdialog", "xdialog"}, dialogProgs...)
	}
	if p, err := findProgram(dialogProgs); err == nil {
		dialogProg = p
	}
	if dialogProg == "" {
		return false
	}
	return true
}

// Prompt user using 'dialog', or a graphical variant if X11 is detected.
func promptUserDialog(path string, choices []*liber.Metadata) *liber.Metadata {
	promptMutex.Lock()
	defer promptMutex.Unlock()

	args := []string{
		"--title", "Metadata Chooser",
		"--menu", fmt.Sprintf("Possible matches for %s:", path),
		"0", "0", "0",
	}
	for idx, md := range choices {
		args = append(args, strconv.Itoa(idx+1))
		args = append(args, md.String())
	}
	var output bytes.Buffer
	cmd := exec.Command(dialogProg, args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = &output
	if err := cmd.Run(); err != nil {
		// If the user selects 'Cancel', dialog will exit with
		// status 1.
		log.Printf("dialog failed: %v", err)
		return nil
	}
	result, err := strconv.Atoi(strings.TrimSpace(output.String()))
	if err != nil {
		return nil
	}
	return choices[result-1]
}

func promptUser(noninteractive bool) func(string, []*liber.Metadata) *liber.Metadata {
	if noninteractive {
		return func(path string, choices []*liber.Metadata) *liber.Metadata {
			return nil
		}
	}

	if findDialogProg() {
		return promptUserDialog
	}

	return promptUserStdin
}
