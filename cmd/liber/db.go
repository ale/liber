package main

import (
	"context"
	"flag"
	"log"
	"os"

	"github.com/google/subcommands"
)

// Dump the database.
type dumpCommand struct{}

func (c *dumpCommand) Name() string     { return "dump-db" }
func (c *dumpCommand) Synopsis() string { return "Dump the database" }
func (c *dumpCommand) Usage() string {
	return `dump-db
  Dump the database.

  Requires exclusive access to the db, so the HTTP server must not be
  running at the same time.

`
}

func (c *dumpCommand) SetFlags(f *flag.FlagSet) {}

func (c *dumpCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	db := openDB()
	defer db.Close()

	if err := db.Dump(os.Stdout); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

// Restore the database.
type restoreCommand struct{}

func (c *restoreCommand) Name() string     { return "restore-db" }
func (c *restoreCommand) Synopsis() string { return "Restore the database" }
func (c *restoreCommand) Usage() string {
	return `restore-db
  Restore the database.

  Requires exclusive access to the db, so the HTTP server must not be
  running at the same time.

`
}

func (c *restoreCommand) SetFlags(f *flag.FlagSet) {}

func (c *restoreCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	db := openDB()
	defer db.Close()

	if err := db.Restore(os.Stdin); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

// Reindex the database.
type reindexCommand struct{}

func (c *reindexCommand) Name() string     { return "reindex" }
func (c *reindexCommand) Synopsis() string { return "Regenerate the full-text search index" }
func (c *reindexCommand) Usage() string {
	return `reindex
  Regenerate the full-text search index.

`
}

func (c *reindexCommand) SetFlags(f *flag.FlagSet) {}

func (c *reindexCommand) Execute(ctx context.Context, f *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	db := openDB()
	defer db.Close()

	if err := db.Reindex(); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}
